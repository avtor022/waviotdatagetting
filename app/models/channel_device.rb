class ChannelDevice < ApplicationRecord
  belongs_to :channel
  belongs_to :device

  validates :channel_id, uniqueness: { scope: :device_id }

  class << self
    def get_data(http = Net::HTTP.start("lk.curog.ru", 443, :use_ssl => true, read_timeout: 300))
      Modem.where(is_deleted: false).each do |modem|
        tree_item_id = modem.tree_item.id unless modem.tree_item.nil?
        tree_item_data = WaviotApiConnect.new("https://lk.curog.ru/api.data/get_full_element_info/?id=#{tree_item_id}", http).get_data
        if tree_item_data[:status] == 'ok' && tree_item_data[:devices] != [] && !tree_item_data[:devices].nil?
          update_channels(tree_item_data[:devices], modem.devices.first)
        end
      end
    end

    def update_channels(device_from_api, device_db)
      device_from_api.each do |device|
        device[1][:registrators].each do |registrator|
          channel_db = Channel.find_by(title: registrator[1][:channel_id])
          device_db.channel_devices.build(device: device_db, channel: channel_db).save unless channel_db.nil?
        end
      end
    end
  end
end
