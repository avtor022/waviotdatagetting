class Modem < ApplicationRecord
  belongs_to :tree_item
  has_many :devices, dependent: :destroy

  class << self
    def get_data(http = Net::HTTP.start("lk.curog.ru", 443, :use_ssl => true, read_timeout: 300))
      TreeItem.where(is_deleted: false).each do |tree_item|
        item_id = tree_item.id
        modems = WaviotApiConnect.new("https://lk.curog.ru/api.tree/get_modems/?id=#{item_id}", http).get_data[:modems].select { |m| m[:modem_type] == "7" }
        update_modems(modems, tree_item) unless modems.nil? || modems == []
      end
    end

    def update_modems(modems_from_api, tree_item)
      @modems_to_db = []
      element_modems = tree_item.modems
      modems_from_api.each do |modem_from_api|
        @modem_db = element_modems.find_by(modem_api_id: modem_from_api[:id], is_deleted: false)
        modem_full_info = get_modem_full_info(modem_from_api)
        modem_to_db = {
          modem_api_id: modem_from_api[:id],
          modem_type: modem_from_api[:modem_type],
          dl_change_timestamp: modem_from_api[:dl_change_timestamp],
          description: modem_full_info[:description],
          battery: modem_from_api[:battery],
          battery_type: modem_from_api[:battery_type],
          hw_version: modem_from_api[:hw_version],
          sw_version: modem_from_api[:sw_version],
          last_config_time: modem_from_api[:last_config_time],
          last_info_message: modem_from_api[:last_info_message],
          last_station_time: modem_from_api[:last_station_time],
          protocol_id: modem_from_api[:protocol_id],
          temperature: modem_from_api[:temperature],
          transform_current: modem_full_info[:transform][:current],
          transform_voltage: modem_full_info[:transform][:voltage]
        }
        if @modem_db.nil?
          @modems_to_db.push(modem_to_db)
        else
          @modem_db.update_attributes(modem_to_db)
        end
      end
      tree_item.modems.import @modems_to_db unless @modems_to_db == []
    end

    def get_modem_full_info(m)
      modem_id = m[:id]
      url = "https://lk.curog.ru/api.modem/full_info/?id=#{modem_id}&key=#{Rails.configuration.ACCESS_KEY}"
      uri = URI.parse(url)
      eval(Net::HTTP.get(uri).force_encoding("utf-8").gsub(/(null)/, 'nil'))[:info]
    end

    def delete_modem(modem)
      modem.update_attributes(is_deleted: true)
    end
  end
end
