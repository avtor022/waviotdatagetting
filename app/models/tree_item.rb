class TreeItem < ApplicationRecord
  has_many :modems, dependent: :destroy

  class << self
    def get_data(http = Net::HTTP.start("lk.curog.ru", 443, :use_ssl => true, read_timeout: 300))
      tree_items = WaviotApiConnect.new("https://lk.curog.ru/api.tree/get_tree/?id=#{Rails.configuration.ROOT_ID}&fields=
        firstname,middlename,appartment,city,district,street,building,entrance,account,vm_code,ovm_code", http).get_data[:tree]
      tree_from_api_to_db(tree_items)
    end

    def tree_from_api_to_db(tree_items)
      @items_ids = []
      @items_to_db = []
      get_nested_items(tree_items)
      TreeItem.import @items_to_db unless @items_to_db == []
      tree_items_delete()
    end

    def get_nested_items(items,parent = nil)
      items.each do |item|
        item = item[1]
        tree_update(item, parent)
        @items_ids.push(item[:id].to_i)
        #next_level_items - select all nested values with consisting of numbers keys.
        next_level_items = item.select{ |k,v| k[/\d/] }
        get_nested_items(next_level_items, item) unless next_level_items.nil?
      end
    end

    def tree_update(tree_item, parent = nil)
      parent_id = parent[:id] unless parent.nil?
      @item_db = TreeItem.where(id: tree_item[:id]).first
      item_to_db = {
        id: tree_item[:id],
        item_title: tree_item[:name],
        item_type: tree_item[:type],
        firstname: tree_item[:firstname],
        middlename: tree_item[:middlename],
        appartment: tree_item[:appartment],
        city: tree_item[:city],
        district: tree_item[:district],
        street: tree_item[:street],
        building: tree_item[:building],
        entrance: tree_item[:entrance],
        account: tree_item[:account],
        vm_code: tree_item[:vm_code],
        ovm_code: tree_item[:ovm_code],
        parent_id: parent_id
      }
      if @item_db.nil?
        @items_to_db.push(item_to_db)
      else
        @item_db.update_attributes(item_to_db)
      end
    end

    def tree_items_delete
      TreeItem.all.each do |item|
        item.update_attributes(is_deleted: true) unless @items_ids.include?(item.id)
      end
    end
  end
end
