class DValue < ApplicationRecord
  belongs_to :device
  belongs_to :channel

  validates :device_id, uniqueness: { scope: [:channel_id, :val_date]}

  class << self
    def get_data(http = Net::HTTP.start("lk.curog.ru", 443, :use_ssl => true, read_timeout: 300))
      devices = Device.where(is_deleted: false)
      time_end = DateTime.now.beginning_of_hour.to_i
      devices.each do |device|
        modem_db = device.modem
        device.channels.each do |channel|
          value_date_max = device.d_values.max_by { |value| value.val_date }.val_date.to_i unless device.d_values == []
          time_start = value_date_max || time_end - 5.years
          item_values = WaviotApiConnect.new("https://lk.curog.ru/api.data/get_modem_channel_values/?modem_id=#{modem_db.modem_api_id}
                                                 &channel=#{channel.title}&from=#{time_start}&to=#{time_end}", http).get_data
          if item_values[:status] == 'ok' && item_values[:values] != [] && !item_values[:values].nil?
            update_values(item_values[:values], device, channel.id)
          end
        end
      end
    end

    def update_values(item_values, device, channel_id)
      values_to_db = []
      item_values.each do |item_value|
        val_date = Time.at(item_value[0].to_s.to_i).beginning_of_hour + 11.hours
        values_to_db.push({
          channel_id: channel_id,
          val_date: val_date,
          dev_value: item_value[1]
        })
      end
      device.d_values.import values_to_db unless values_to_db == []
    end
  end

end
