class DEvent < ApplicationRecord
  belongs_to :device

  class << self
    def get_data(http = Net::HTTP.start("lk.curog.ru", 443, :use_ssl => true, read_timeout: 300))
      Device.where(is_deleted: false).each do |device|
        modem = device.modem
        d_events = WaviotApiConnect.new("https://lk.curog.ru/api.data/get_events/?modem_id=#{modem.modem_api_id}", http).get_data[:events]
        update_events(device, d_events) unless d_events.nil? || d_events == []
      end
    end

    def update_events(device, d_events)
      @events_to_db = []
      d_events.each do |d_event|
        @event_db = device.d_events.find_by(event_api_id: d_event[0])
        event_to_db = {
          event_api_id: d_event[0],
          event_type: d_event[1][:event_type],
          primary_code: d_event[1][:code][:primary].to_i,
          secondary_code: d_event[1][:code][:secondary].to_i,
          decimal_code: d_event[1][:code][:decimal].to_i,
          hex_code: d_event[1][:code][:hex],
          param: d_event[1][:param],
          description: d_event[1][:description],
          event_date: d_event[1][:date_time]
        }
        if @event_db.nil?
          @events_to_db.push(event_to_db)
        else
          @event_db.update(event_to_db)
        end
      end
      device.d_events.import @events_to_db unless @events_to_db == []
    end
  end
end
