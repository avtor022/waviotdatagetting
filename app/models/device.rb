class Device < ApplicationRecord
  belongs_to :modem
  has_many :channel_devices, dependent: :destroy
  has_many :channels, through: :channel_devices
  has_many :d_values, dependent: :destroy
  has_many :d_events, dependent: :destroy

  class << self
    def get_data(http = Net::HTTP.start("lk.curog.ru", 443, :use_ssl => true, read_timeout: 300))
      Modem.where(is_deleted: false).each do |modem|
        tree_item_id = modem.tree_item.id unless modem.tree_item.nil?
        tree_item_data = WaviotApiConnect.new("https://lk.curog.ru/api.data/get_full_element_info/?id=#{tree_item_id}", http).get_data
        if tree_item_data[:status] == 'ok' && tree_item_data[:devices] != [] && !tree_item_data[:devices].nil?
          update_devices(tree_item_data[:devices], modem)
        end
      end
    end

    def update_devices(devices_from_api, modem)
      devices_to_db = []
      devices_from_api.each do |d|
        device_db = modem.devices.find_by(device_sn: d[1][:device_sn], is_deleted: false)
        if device_db.nil? && d[1][:modem_id] == modem.modem_api_id
          devices_to_db.push({device_sn: d[1][:device_sn]})
        elsif !device_db.nil? && d[1][:modem_id] != modem.modem_api_id
          delete_device(device_db)
        end
      end
      modem.devices.import devices_to_db unless devices_to_db == []
    end

    def delete_device(device)
      device.update_attributes(is_deleted: true)
    end
  end

end
