class Channel < ApplicationRecord
  has_many :channel_devices, dependent: :destroy
  has_many :devices, through: :channel_devices
  has_many :d_values, dependent: :destroy
end
