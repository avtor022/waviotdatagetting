class WaviotDataGettingJob < ApplicationJob
  queue_as :default

  def perform(*args)
    TreeItem.get_data(http)
    Modem.get_data(http)
    Device.get_data(http)
    ChannelDevice.get_data(http)
    DValue.get_data(http)
    DEvent.get_data(http)
  end
end
