namespace :channels do
  desc "TODO"
  task channels_adding: :environment do
    channel_titles = [
      "electro_ac_p_lsum_t1",
      "electro_ac_p_lsum_t2",
      "electro_ac_p_lsum_t3",
      "electro_ac_p_lsum_t4",
      "electro_ac_p_lsum_tsum",
      "electro_ac_m_lsum_t1",
      "electro_ac_m_lsum_t2",
      "electro_ac_m_lsum_t3",
      "electro_ac_m_lsum_t4",
      "electro_ac_m_lsum_tsum",
      "electro_re_p_lsum_t1",
      "electro_re_p_lsum_t2",
      "electro_re_p_lsum_t3",
      "electro_re_p_lsum_t4",
      "electro_re_p_lsum_tsum",
      "electro_re_m_lsum_t1",
      "electro_re_m_lsum_t2",
      "electro_re_m_lsum_t3",
      "electro_re_m_lsum_t4",
      "electro_re_m_lsum_tsum"
    ]
    channels_array = channel_titles.map { |channel| {title: channel}}
    Channel.create(channels_array)
  end

end
