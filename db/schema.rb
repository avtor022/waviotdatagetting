# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_06_032653) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "channel_devices", force: :cascade do |t|
    t.bigint "channel_id"
    t.bigint "device_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["channel_id"], name: "index_channel_devices_on_channel_id"
    t.index ["device_id"], name: "index_channel_devices_on_device_id"
  end

  create_table "channels", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "d_events", force: :cascade do |t|
    t.bigint "device_id"
    t.string "event_api_id"
    t.string "event_type"
    t.integer "primary_code"
    t.integer "secondary_code"
    t.integer "decimal_code"
    t.string "hex_code"
    t.string "param"
    t.string "description"
    t.string "event_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["device_id"], name: "index_d_events_on_device_id"
  end

  create_table "d_values", force: :cascade do |t|
    t.datetime "val_date"
    t.float "dev_value"
    t.bigint "device_id"
    t.bigint "channel_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["channel_id"], name: "index_d_values_on_channel_id"
    t.index ["device_id"], name: "index_d_values_on_device_id"
  end

  create_table "devices", force: :cascade do |t|
    t.string "device_sn"
    t.bigint "modem_id"
    t.boolean "is_deleted", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["modem_id"], name: "index_devices_on_modem_id"
  end

  create_table "modems", force: :cascade do |t|
    t.string "modem_api_id"
    t.integer "modem_type"
    t.integer "dl_change_timestamp"
    t.float "battery"
    t.integer "battery_type"
    t.string "hw_version"
    t.string "sw_version"
    t.integer "last_config_time"
    t.integer "last_info_message"
    t.integer "last_station_time"
    t.integer "protocol_id"
    t.integer "temperature"
    t.bigint "tree_item_id"
    t.boolean "is_deleted", default: false, null: false
    t.integer "transform_current"
    t.integer "transform_voltage"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tree_item_id"], name: "index_modems_on_tree_item_id"
  end

  create_table "tree_items", force: :cascade do |t|
    t.string "item_title"
    t.string "item_type"
    t.integer "parent_id"
    t.boolean "is_deleted", default: false, null: false
    t.string "firstname"
    t.string "middlename"
    t.string "appartment"
    t.string "city"
    t.string "district"
    t.string "street"
    t.string "building"
    t.string "entrance"
    t.string "account"
    t.string "vm_code"
    t.string "ovm_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
