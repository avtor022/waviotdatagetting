class CreateDevices < ActiveRecord::Migration[6.0]
  def change
    create_table :devices do |t|
      t.string :device_sn
      t.references :modem, index: true
      t.boolean :is_deleted, null: false, default: false

      t.timestamps
    end
  end
end
