class CreateModems < ActiveRecord::Migration[6.0]
  def change
    create_table :modems do |t|
      t.string :modem_api_id
      t.integer :modem_type
      t.integer :dl_change_timestamp
      t.float :battery
      t.integer :battery_type
      t.string :hw_version
      t.string :sw_version
      t.integer :last_config_time
      t.integer :last_info_message
      t.integer :last_station_time
      t.integer :protocol_id
      t.integer :temperature
      t.references :tree_item, index: true
      t.boolean :is_deleted, null: false, default: false
      t.integer :transform_current
      t.integer :transform_voltage
      t.string  :description

      t.timestamps
    end
  end
end
