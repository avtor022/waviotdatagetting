class CreateTreeItems < ActiveRecord::Migration[6.0]
  def change
    create_table :tree_items do |t|
      t.string :item_title
      t.string :item_type
      t.integer :parent_id
      t.boolean :is_deleted, null: false, default: false
      t.string :firstname
      t.string :middlename
      t.string :appartment
      t.string :city
      t.string :district
      t.string :street
      t.string :building
      t.string :entrance
      t.string :account
      t.string :vm_code
      t.string :ovm_code

      t.timestamps
    end
  end
end
