class CreateChannelDevices < ActiveRecord::Migration[6.0]
  def change
    create_table :channel_devices do |t|
      t.references :channel, index: true
      t.references :device, index: true

      t.timestamps
    end
  end
end
