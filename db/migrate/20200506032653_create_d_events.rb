class CreateDEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :d_events do |t|
      t.references :device, index: true
      t.string :event_api_id
      t.string :event_type
      t.integer :primary_code
      t.integer :secondary_code
      t.integer :decimal_code
      t.string :hex_code
      t.string :param
      t.string :description
      t.string :event_date

      t.timestamps
    end
  end
end
