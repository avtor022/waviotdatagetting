class CreateDValues < ActiveRecord::Migration[6.0]
  def change
    create_table :d_values do |t|
      t.timestamp :val_date
      t.float :dev_value
      t.references :device, index: true
      t.references :channel, index: true


      t.timestamps
    end
    execute('ALTER TABLE d_values ALTER COLUMN id SET DATA TYPE BIGINT')
  end
end
